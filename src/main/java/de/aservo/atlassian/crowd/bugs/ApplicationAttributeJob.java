package de.aservo.atlassian.crowd.bugs;

import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.config.Schedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Map;

@Component
public class ApplicationAttributeJob implements JobRunner {

    private static final Logger log = LoggerFactory.getLogger(ApplicationAttributeJob.class);

    private static final JobId JOB_ID = JobId.of(ApplicationAttributeJob.class.getSimpleName() + "Id");
    private static final JobRunnerKey JOB_RUNNER_KEY = JobRunnerKey.of(ApplicationAttributeJob.class.getSimpleName() + "Runner");

    @ComponentImport
    private final ApplicationManager applicationManager;

    @ComponentImport
    private final SchedulerService schedulerService;

    /**
     * Constructor.
     *
     * @param applicationManager injected {@link ApplicationManager}
     * @param schedulerService   injected {@link SchedulerService}
     */
    @Autowired
    public ApplicationAttributeJob(
            final ApplicationManager applicationManager,
            final SchedulerService schedulerService) {

        this.applicationManager = applicationManager;
        this.schedulerService = schedulerService;
    }

    /**
     * Register this {@link JobRunner}
     */
    @PostConstruct
    public void register() {
        final JobConfig jobConfig = getJobConfig();
        log.info("Register job key '{}'", JOB_RUNNER_KEY);
        schedulerService.registerJobRunner(JOB_RUNNER_KEY, this);

        try {
            log.info("Schedule job id '{}' with type {}", JOB_ID, jobConfig.getSchedule().getType());
            schedulerService.scheduleJob(JOB_ID, jobConfig);
        } catch (SchedulerServiceException e) {
            log.info("Scheduling job id '{}' failed ({})", JOB_ID, e.getMessage());
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Unregister this {@link JobRunner}
     */
    @PreDestroy
    public void unregister() {
        log.info("Unregister job key '{}'", JOB_RUNNER_KEY);
        schedulerService.unregisterJobRunner(JOB_RUNNER_KEY);
        log.info("Unschedule job id '{}'", JOB_ID);
        schedulerService.unscheduleJob(JOB_ID);
    }

    @Nullable
    @Override
    public JobRunnerResponse runJob(
            final JobRunnerRequest request) {

        log.info("Start job");
        log.info("");

        final List<Application> applications = applicationManager.findAll();

        for (final Application application : applications) {
            log.info("Application: {}", application.getName());
            log.info("Attributes:");

            for (Map.Entry<String, String> attribute : application.getAttributes().entrySet()) {
                log.info("  {}: {}", attribute.getKey(), attribute.getValue());
            }

            log.info("");
        }

        log.info("End job");
        log.info("");

        return JobRunnerResponse.success();
    }

    private JobConfig getJobConfig() {
        return JobConfig
                .forJobRunnerKey(JOB_RUNNER_KEY)
                .withRunMode(RunMode.RUN_LOCALLY)
                .withSchedule(Schedule.forCronExpression("0/10 * * ? * * *"));
    }

}
